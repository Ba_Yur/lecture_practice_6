import 'dart:math';

import 'main.dart' as math;
import 'package:flutter/cupertino.dart';

class CounterProvider extends ChangeNotifier {

  int _counter1 = 0;
  int get counter1 {
    return _counter1;
  }

  int _counter2 = 0;
  int get counter2 {
    return _counter2;
  }

  void IncreaseCounter() {
    bool random = Random().nextBool();
    random ? _counter1++ : _counter2++;

    notifyListeners();
  }

  void DecreaseCounter() {
    bool random = Random().nextBool();
    random ? _counter1-- : _counter2--;

    notifyListeners();
  }

}