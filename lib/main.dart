import 'package:flutter/material.dart';
import 'package:lecture_practice_6/counter_provider.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => CounterProvider(),
      child: MaterialApp(
        home: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    CounterProvider counterProvider = Provider.of<CounterProvider>(context, listen: false);
    return Scaffold(
      appBar: AppBar(
        title: Text('Lecture Practice 6'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Selector<CounterProvider, int>(
            selector: (_, counterProvider) => counterProvider.counter1,
            builder: (context, counter1, child) {
              return Text('Counter 1: $counter1',
              style: TextStyle(
                fontSize: 40.0,
              ),);
            },
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                onPressed: counterProvider.IncreaseCounter,
                child: Text('Increment'),
              ),
              ElevatedButton(
                onPressed: counterProvider.DecreaseCounter,
                child: Text('Decrement'),
              ),
            ],
          ),
          Selector<CounterProvider, int>(
            selector: (_, counterProvider) => counterProvider.counter2,
            builder: (context, counter2, child) {
              return Text('Counter 2: $counter2',
              style: TextStyle(
                fontSize: 40.0,
              ),);
            },
          ),
        ],
      ),
    );
  }
}
